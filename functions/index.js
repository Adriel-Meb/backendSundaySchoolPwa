
const functions = require('firebase-functions');

//******firebase admin SDK*****
const admin = require("firebase-admin");
var serviceAccount = require("./permissions.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sundayschoolrestapi.firebaseio.com"
});


const express = require('express');
const app = express();
const db = admin.firestore();

const cors = require('cors');
app.use(cors({ origin: true }));



//Routes
app.get('/hello', (req, res) => {
    return res.status(200).send('HELLO WORLD!its Adriel.......');
});




//Create
    //POST
app.post('/api/create', (req, res) => {
    
    (async () => {

        try {
            
            await db.collection('lessons').doc('/' + req.body.id + '/')  
                .create({

                    title: req.body.title,
                    activities:req.body.activities,
                    scripture:req.body.scripture,
                        ages:req.body.ages,
                    takeaway:req.body.takeaway,
                    memoryverse: req.body.memoryverse,
                    date:req.body.date
                    
                    })
            
            return res.status(200).send();

        }

        catch (error)
        {

            console.log(error);
            return res.status(500).send(error);
        }

        
    })();

});



//Read a SPECIFIC PRODUCT BASED ID
    //GET
app.get('/api/read/:id', (req, res) => {
    
    (async () => {

        try
        {
            const document = db.collection('lessons').doc(req.params.id);
            let product = await document.get();
            let response = product.data();
 
            return res.status(200).send(response);

        }

        catch (error)
        {

            console.log(error);
            return res.status(500).send(error);
        }

        
    })();

});


//Read ALL PRODUCT BASED ID
    //GET
app.get('/api/read', (req, res) => {
    
    (async () => {

        try
        {
            let query = db.collection('lessons');
            let response = [];

            await query.get().then(querySnapshot => {
                let docs = querySnapshot.docs; // the result of the query

                for (let doc of docs)
                {
                    const selectedItem = {
                        title: doc.data().title,
                        activities: doc.data().activities,
                        scripture: doc.data().scripture,
                        ages: doc.data().ages,
                        takeaway: doc.data().takeaway,
                        memoryverse: doc.data().memoryverse,
                        date: doc.data().date
                    };
                    response.push(selectedItem);
                }
                return response; // each then should return a value
                
            })
            return res.status(200).send(response);


        }

        catch (error)
        {

            console.log(error);
            return res.status(500).send(error);
        }

        
    })();

});




//Update
    //PUT
app.put('/api/update/:id', (req, res) => {
    
    (async () => {

        try
        {
            const document = db.collection('lessons').doc(req.params.id);

            await document.update({

                title: req.body.title,
                activities: req.body.activities,
                scripture: req.body.scripture,
                ages: req.body.ages,
                takeaway: req.body.takeaway,
                memoryverse: req.body.memoryverse,
                date: req.body.date
            });
                
            return res.status(200).send();

        }

        catch (error)
        {

            console.log(error);
            return res.status(500).send(error);
        }

        
    })();

});






//DELETE
app.delete('/api/delete/:id', (req, res) => {
    
    (async () => {

        try
        {
            const document = db.collection('lessons').doc(req.params.id);

            await document.delete();
                
            return res.status(200).send();

        }

        catch (error)
        {

            console.log(error);
            return res.status(500).send(error);
        }

        
    })();

});























exports.app = functions.https.onRequest(app);